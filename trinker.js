const { filter, flat, includes } = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },
    // plus //


    allMale: function(p){
        return p.filter((person) => (person.gender == "Male"));
    },

    allFemale: function(p){
        return p.filter((person) => (person.gender == "Female"));
    },

    nbOfMale: function(p){
        return this.allMale(p).length
    },

    nbOfFemale: function(p){
        return this.allFemale(p).length;
    },

    nbOfMaleInterest: function(p){
       return p.filter((person) => (person.looking_for == "M")).length;
    },

    nbOfFemaleInterest: function(p){
        return p.filter((person) => (person.looking_for == "F")).length;
    },
    
    nb0GagnentPlus2000: function(p){
        return p.filter((person) => (person.income.substring(1) > 2000)).length;
    },
    nb0AimentDrama: function(p){
        return p.filter((person) => person.pref_movie .includes("Drama")).length;
    },
    nbFemmesScienceFiction: function(p){
        return p.filter((person) =>  person.pref_movie .includes("Sci-Fi")  && (person.gender == "Female")).length;
    },
    nbAimentDocumentaires1482: function(p){
        return p.filter((person) =>  person.pref_movie .includes("Documentary") && (person.income.substring(1) > 1482)).length;
    },
    listNomPrenomIdPlus4000:function(p){
        let ganger = []
        console.log(ganger)

        let ger = p.filter(function(person){
           console.log(">>>>" + person.income.substring(1))
            return person.income.substring(1) > 4000;
        });
        
        ger.forEach(Element=>{
            let obj = {
               "name": Element.first_name,
               "lastname":Element.last_name,
               "id": Element.id,
                "Salary":Element.income,
            };
            ganger.push(obj) 
        });
        return  ganger 
    },
    hommeRich:function(p){
        
        let max = 0
       let rich ; 
      // return rich
      this.allMale(p).forEach(Salary => {
           let salaries =  parseFloat(Salary.income.substring(1)); 
            if (salaries > max){
                max = salaries
                rich = Salary;
           }
       });
       return max +' ' + rich.last_name +' '+ rich.id
        },

    salaryMoyen:function(p){
       let sum = []

       let id  = []
       
         let moyen = p.filter(person => parseFloat(person.income.substring(1)))
             moyen.forEach(element=>{
             sum.push( parseFloat(element.income.substring(1)))
       })
       moyen.forEach(element =>{
            id.push(element.id)
       })
          let total = (a,b)=>a+b
       return sum.reduce(total)/id.length
     
    },
    salaryMedian:function(p){
        let med =[]

        let id =[]

        let median = p.filter(person => parseFloat(person.income.substring(1)))
        median.forEach(Element =>{
            med.push(parseFloat(Element.income.substring(1)))
        })
       // median.forEach(element =>{
            //id.push(element.id)
       // })
        let tout = (a,b)=> a+b
        return (med.reduce((a,b) => a+b)) /p.length
    },

    nord:function(p){
      return p.filter(person => person.latitude > 0).length
    },
    salarySud: function(p){
        let mon = 0
        
        
         p.filter(person => person.latitude < 0).length
         /*
           let max = 0
       let rich ; 
      // return rich
      this.allMale(p).forEach(Salary => {
           let salaries =  parseFloat(Salary.income.substring(1)); 
            if (salaries > max){
                max = salaries
                rich = Salary;
           }
       });
       return max +' ' + rich.last_name +' '+ rich.id
         */

    },

    match: function(p){
        return "not implemented".red;
    },
}